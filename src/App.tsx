import React from 'react';
import './App.css';
import {
  BrowserRouter,
  Route,
  Switch,
} from 'react-router-dom';
import Layout from './components/layout';
import {
  Provider,
  } from 'react-redux';
import store from './stores';
import LoginScreen from './screens/loginScreen';

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route
            path='/login'
            component={LoginScreen}
          />
          <Route
            path='/'
            component={Layout}
          />
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
