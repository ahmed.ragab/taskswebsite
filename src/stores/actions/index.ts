import {
  TaskPayload,
  DELETE_TASK,
  Category,
  FETCH_TASKS,
  FILTER_TASKS,
} from "../types";
import {
  TasksActions,
  ADD_TASK,
} from "../types";
import Axios from "axios";

export function fetchTasks(tasks: Category[]): TasksActions {
  return {
    type: FETCH_TASKS,
    payload: tasks,
  }
}

export const fetchAllTasks = () => {
  return (dispatch: any) => {
    return Axios.get('taskData.json')
      .then(response => {
        dispatch(fetchTasks(response.data))
      })
      .catch(error => {
        throw(error);
      });
  };
};

export function addTask(newTask: TaskPayload): TasksActions {
  return {
    type: ADD_TASK,
    payload: newTask,
  }
}

export function deleteTask(deletedTask: TaskPayload): TasksActions {
  return {
    type: DELETE_TASK,
    payload: deletedTask,
  }
}

export function filterTasks(startDate: number, endDate: number): TasksActions {
  return {
    type: FILTER_TASKS,
    payload: {
      startDate,
      endDate,
    },
  }
}