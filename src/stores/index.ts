import {
  createStore, applyMiddleware, combineReducers,
} from "redux";
import thunk from 'redux-thunk';
import tasksReducer from './reducers/index';

const store = createStore(combineReducers({tasksReducer}), applyMiddleware(thunk));

export default store;