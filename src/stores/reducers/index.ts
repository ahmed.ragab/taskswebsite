import {
  TasksActions,
  TasksState,
  DELETE_TASK,
  Category,
  Task,
  ADD_TASK,
  FETCH_TASKS,
  FILTER_TASKS,
} from "../types";

const tasksState: TasksState = {
  categories: [],
  filteredCategories: [],
};

function tasksReducer(state = tasksState, action: TasksActions) {
  if (action.type === DELETE_TASK) {
    const {
      name,
      category,
    } = action.payload;
    const newList = state.filteredCategories.map((singleCategory: Category) => {
      if (singleCategory.name === category) {
        const newTasks = singleCategory.tasks.filter((task: Task) => {
          return task.name !== name;
        });
        return {
          name: singleCategory.name,
          tasks: newTasks,
        }
      } else {
        return singleCategory;
      }
    });
    const newCategoryList = state.categories.map((singleCategory: Category) => {
      if (singleCategory.name === category) {
        const newTasks = singleCategory.tasks.filter((task: Task) => {
          return task.name !== name;
        });
        return {
          name: singleCategory.name,
          tasks: newTasks,
        }
      } else {
        return singleCategory;
      }
    });
    return {
      ...state,
      categories: newCategoryList,
      filteredCategories: newList,
    };
  } else if (action.type === ADD_TASK) {
    const {
      category,
    } = action.payload;
    let categoryFound = false;
    const newList = state.categories.map((singleCategory: Category) => {
      if (singleCategory.name === category) {
        categoryFound = true;
        const newTasks = [...singleCategory.tasks , {
          ...action.payload,
          date: new Date().getTime(),
        }];
        return {
          name: singleCategory.name,
          tasks: newTasks,
        };
      } else {
        return singleCategory;
      }
    });
    if (!categoryFound) {
      return {
        ...state,
        categories: [...state.categories, {
          name: category,
          tasks: [{
            ...action.payload,
            date: new Date().getTime(),
          }],
        }],
      }
    }
    return {
      ...state,
      categories: newList,
    };
  } else if (action.type === FETCH_TASKS) {
    const startDate = new Date();
    let endDate: Date = new Date();
    endDate.setDate(startDate.getDate() + 1);
    const newList = action.payload.map((singleCategory: Category) => {
      const newTasks = singleCategory.tasks.filter((task: Task) => {
        if (task.date) {
          return task.date >= startDate.getTime() && task.date <= endDate.getTime();
        } else {
          return true;
        }
      });
      return {
        name: singleCategory.name,
        tasks: newTasks,
      }
    })
    return {
      ...state,
      categories: action.payload,
      filteredCategories: newList,
    };
  } else if (action.type === FILTER_TASKS) {
    const {
      startDate,
      endDate,
    } = action.payload;
    const newList = state.categories.map((singleCategory: Category) => {
      const newTasks = singleCategory.tasks.filter((task: Task) => {
        if (task.date) {
          return task.date >= startDate && task.date <= endDate;
        }
        return true;
      });
      return {
        name: singleCategory.name,
        tasks: newTasks,
      }
    })
    return {
      ...state,
      filteredCategories: newList,
    };
  }
  return state;
};

export default tasksReducer;