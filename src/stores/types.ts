export const ADD_TASK = 'ADD_TASK';
export const DELETE_TASK = 'DELETE_TASK';
export const FETCH_TASKS = 'FETCH_TASKS';
export const FILTER_TASKS = 'FILTER_TASKS';

interface AddTask {
  type: typeof ADD_TASK;
  payload: Task;
}

interface DeleteTask {
  type: typeof DELETE_TASK;
  payload: Task;
}

export interface FetchTaks {
  type: typeof FETCH_TASKS;
  payload: Category[];
}

export interface Category {
  name: string;
  tasks: Task[];
}

export interface Task {
  name: string;
  category: string;
  date?: number;
}

export interface TasksState {
  categories: Category[];
  filteredCategories: Category[];
}

export interface TaskPayload {
  name: string;
  category: string;
}

export interface FilterTasks {
  type: typeof FILTER_TASKS;
  payload: any;
}


export type TasksActions = AddTask | DeleteTask | FetchTaks | FilterTasks;