import React, {
  ChangeEvent,
  FormEvent,
} from 'react';
import styled from 'styled-components';
import {
  TaskPayload,
} from '../../stores/types';
import {
  Button,
} from '../sharedComponents';


interface State {
  category: string;
  task: string;
}

interface Props {
  handleAddTask: (task: TaskPayload) => void;
}

const Form = styled.form`
  margin-bottom: 40px;
  display: flex;
  flex-direction: column;
  algin-items: center;
  background-color: black;
  width: 100%;
  border-radius: 5px;
`;

const Input = styled.input`
  border-radius: 4px;
  padding: 5px;
  border: none;
`;

const Label = styled.label`
  margin: 0 10px;
  color: white;
`;

const FieldsContainer = styled.div`
  display: flex;
  flex-direction: row;
  @media(max-width: 620px) {
    flex-direction: column;
  }
`;

const FieldInput = styled.div`
  margin: 10px;
`;
class AddTaskForm extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      category: '',
      task: '',
    }
  }


  onSubmit = (event: FormEvent<HTMLFormElement>) => {
    const {
      category,
      task,
    } = this.state;
    event.preventDefault();
    if (category.length > 0 && task.length > 0) {
      const payload: TaskPayload = {
        name: task,
        category,
      }
      this.props.handleAddTask(payload);
      this.setState({
        category: '',
        task: '',
      })
    }
  }
  render() {
    const {
      category,
      task,
    } = this.state;
    return (
      <Form onSubmit={this.onSubmit}>
        <FieldsContainer>
          <FieldInput>
            <Label>
              Category :
          </Label>
            <Input
              type='text'
              value={category}
              onChange={(event: ChangeEvent<HTMLInputElement>) => this.setState({
                category: event.target.value,
              })}
            />
          </FieldInput>
          <FieldInput>
            <Label>
              Task :
          </Label>
            <Input
              type='text'
              value={task}
              onChange={(event: ChangeEvent<HTMLInputElement>) => this.setState({
                task: event.target.value,
              })}
            />
          </FieldInput>
        </FieldsContainer>
        <Button
          type='submit'
        >
          Add Task
        </Button>
      </Form>
    );
  }
}

export default AddTaskForm;