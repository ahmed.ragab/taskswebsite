import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const Header = styled.header`
  width: 100%;
  height: 65px;
  position: fixed;
  background-color: black;
  top:0;
  left: 0;
`;

const Nav = styled.nav`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0 1rem;
  height: 100%;
  justify-content: space-between;
`;

const Logo = styled.div`
  font-size: 1.3rem;
  margin-left: 20px;
  color: white;
`;

const NavigationListContainer = styled.div`
  @media(max-width: 465px) {
    display: none;
  }
`;

const LogoContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const NavigationList = styled.ul`
  display: flex;
  flex-direction: row;
  list-style: none;
  padding: 0;
  margin: 0;
`;

const NavigationItem = styled.li`
  padding: 0 1rem;
  a {
    text-decoration: none;
    color: white;
    &:active , &:hover {
      color: grey;
    }
  }
`;

const DrawerButtonContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  height: 24px;
  width: 30px;
  background: transparent;
  border: none;
  cursor: pointer;
  padding: 0;
  box-sizing: border-box;
  @media(min-width: 465px) {
    display: none;
  }
`;

const DrawerButtonLine = styled.div`
  width: 30px;
  height: 2px;
  background-color: white;
`;

class Navbar extends React.Component {

  render() {
    return (
      <Header>
        <Nav>
          <LogoContainer>
            <DrawerButtonContainer>
              <DrawerButtonLine />
              <DrawerButtonLine />
              <DrawerButtonLine />
            </DrawerButtonContainer>
            <Logo>
              Tasks Website
              </Logo>
          </LogoContainer>
          <NavigationListContainer>
            <NavigationList>
              <NavigationItem>
                <Link
                  to='/'
                >
                  Home
                  </Link>
              </NavigationItem>
              <NavigationItem>
                <Link
                  to='/profile'
                >
                  Profile
                  </Link>
              </NavigationItem>
              <NavigationItem>
                <Link
                  to='/tasks'
                >
                  Tasks
                  </Link>
              </NavigationItem>
            </NavigationList>
          </NavigationListContainer>
        </Nav>
      </Header>
    );
  }
}

export default Navbar;