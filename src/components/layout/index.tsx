import React from 'react';
import {
  Route,
  Switch,
} from 'react-router-dom';
import styled from 'styled-components';
import Navbar from '../navbar';
import AuthenticationRoute from '../authenticationRoute';
import MainScreen from '../../screens/mainScreen';
import TasksScreen from '../../screens/tasksScreen';
import ProfileScreen from '../../screens/profileScreen';

const MainSection = styled.div`
  width: 100%;
`;

class Layout extends React.Component {

  isAuthenticated = localStorage.getItem('credentials');
  render () {
    return (
      <MainSection>
        <Navbar/>
        <Switch>
          <Route
            path='/'
            exact
            component={MainScreen}
          />
          <Route
            path='/tasks'
            exact
            component={TasksScreen}
          />
          <AuthenticationRoute
            hasPermission={Boolean(this.isAuthenticated)}
            component={ProfileScreen}
            path={'/profile'}
            exact
          />
        </Switch>
      </MainSection>
    );
  }
}

export default Layout;