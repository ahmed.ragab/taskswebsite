import React from 'react';
import {
  Task,
  Category,
  TaskPayload,
} from '../../stores/types';
import styled from 'styled-components';

interface Props {
  categories: Category[]; 
  filteredCategories: Category[];
  handleDeleteTask: (task: TaskPayload) => void;
}

const TasksListContainer = styled.div`
  border-radius: 5px;
  background: black; 
  width: 100%;
  padding: 10px;
`;

const CategoryItem = styled.div`

`;

const CategoryName = styled.p`
  font-size: 20px;
  color: white;
`;

const TasksList = styled.ul`
  list-style: none;
`;

const TaskItem = styled.li`
  color: white;
  width: 80%;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const Delete = styled.div`
  margin: 0 auto;
  color: white;
  width: 20%;
  &:hover {
    cursor: pointer;
    color: red;
  }
`;

const TaskContainer = styled.div`
  display: flex;
  margin: 10px;
  width: 100%;
`;

class TasksListComponent extends React.PureComponent<Props> {

  fillTaks = () => {
    return this.props.filteredCategories.map((category: Category, index: number) => {
      if (category.tasks.length > 0) {
        return (
          <CategoryItem key={index.toString()}>
            <CategoryName>
              { category.name }
            </CategoryName>
            <TasksList>
              {category.tasks.map((task: Task, index: number) => {
                return (
                  <TaskContainer key={index.toString()}>
                  <TaskItem>
                    {task.name}
                  </TaskItem>
                  <Delete
                    onClick={() => this.props.handleDeleteTask(task)}
                  >
                    Delete
                  </Delete>
                  </TaskContainer>
                )
              })}
            </TasksList>
          </CategoryItem>
          ) 
      } else {
        return null;
      }
    })
  }

  render() {
    return (
      <TasksListContainer>
        {
          this.fillTaks()
        }
      </TasksListContainer>
    );
  }
}

export default TasksListComponent;