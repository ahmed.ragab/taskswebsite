import React, {
    Component,
    ComponentClass,
    FunctionComponent,
  } from 'react';
  import {
    Redirect,
    Route,
  } from 'react-router-dom';
  
  interface Props {
    component: ComponentClass<any> | FunctionComponent<any>;
    hasPermission: boolean;
    path: string;
    exact: boolean;
  }
  class AuthenticationRoute extends Component<Props> {
    render() {
      const {
        hasPermission,
        component,
        exact,
        path,
      } = this.props;
      if (!hasPermission) {
        return <Redirect to={'/login'} />;
      }
      return (
        <Route
          exact={exact}
          path={path}
          component={component}
        />
      );
    }
  }
  
  export default AuthenticationRoute;
  