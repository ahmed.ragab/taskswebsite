import styled from 'styled-components';
import image from '../assets/images/login.jpg';

export const HomePageBackground = styled.div`
  background-image: url('${image}');
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;
  background-attachment: fixed;
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const Button = styled.button`
  font-size: 1em;
  margin: 1em;
  padding: 0.5em 4em;
  border-radius: 3px;
  width: auto;
  border: none;
  &:hover {
    background: grey;
    cursor: pointer;
  }
`;