import React from 'react';
import {
  connect,
} from 'react-redux';
import {
  Category,
  TaskPayload,
} from '../../stores/types';
import styled from 'styled-components';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import TasksListComponent from '../../components/tasksListComponent';
import AddTaskForm from '../../components/addTaskForm';
import {
  addTask,
  deleteTask,
  fetchAllTasks,
  filterTasks,
} from '../../stores/actions';
import {
  HomePageBackground,
} from '../../components/sharedComponents';

interface Props {
  categories: Category[];
  filteredCategories: Category[];
  addTask: (task: TaskPayload) => object;
  deleteTask: (task: TaskPayload) => object;
  fetchTasks: () => any;
  filterTasks: (startDate: number, endDate: number) => any;
}

const TasksContainer = styled.div`
  margin-top: 65px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 30%
  @media(max-width: 620px) {
    width: 90%;
  }
`;
interface State {
  selectedDate: Date,
}
class TasksScreen extends React.PureComponent<Props, State> {

  constructor (props: Props) {
    super(props);
    this.state = {
      selectedDate: new Date(),
    }
  }

  handleDeleteButtonClick = (task: TaskPayload) => {
    this.props.deleteTask(task);
  };

  handleButtonClick = async (task: TaskPayload) => {
    this.props.addTask(task);
  };

  handleDateChange = (date: Date) => {
    this.setState({
      selectedDate: date,
    });
    const startDay = date;
    let endDate: Date = new Date();
    endDate.setDate(date.getDate() + 1);
    this.props.filterTasks(startDay.getTime(), endDate.getTime());
  }
  
  componentDidMount () {
    this.props.fetchTasks();
  }

  render() {
    return (
      this.props.categories ? 
      <HomePageBackground>
        <TasksContainer>
          <DatePicker
            selected={this.state.selectedDate}
            onChange={this.handleDateChange}
          />
          <AddTaskForm
            handleAddTask={(task: TaskPayload) => this.handleButtonClick(task)}
          />
          <TasksListComponent
            categories={this.props.categories}
            filteredCategories={this.props.filteredCategories}
            handleDeleteTask={this.handleDeleteButtonClick}
          />
        </TasksContainer>
      </HomePageBackground> : null
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    categories: state.tasksReducer.categories,
    filteredCategories: state.tasksReducer.filteredCategories,
  };
};

const MapDispatchToProps = (dispatch: any) => ({
  addTask: (newTask: TaskPayload) => dispatch(addTask(newTask)),
  deleteTask: (deletedTask: TaskPayload) => dispatch(deleteTask(deletedTask)),
  filterTasks: (startDate: number, endDate: number) => dispatch(filterTasks(startDate, endDate)),
  fetchTasks: () => dispatch(fetchAllTasks())
});

export default connect(
  mapStateToProps,
  MapDispatchToProps,
)(TasksScreen);