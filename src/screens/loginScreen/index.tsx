import React, {
  ChangeEvent,
} from 'react';
import styled from 'styled-components';
import {
  authUser,
} from '../../stores/mockdata';
import {
  RouteComponentProps,
  withRouter,
} from 'react-router';
import {
  HomePageBackground,
  Button,
} from '../../components/sharedComponents';

const LoginCard = styled.div`
  width: 30vw;
  height: auto;
  border-radius: 5px;
  padding: 20px;
  background-color: black;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  @media(max-width: 465px) {
    width: 70vw;
  }
`;

const Input = styled.input`
  padding: 1em;
  border-radius: 10px;
  border: none;
  color: white;
  margin-top: 10px;
  background-color: rgba(255,255,255, 0.25);
  width: 26vw;
  @media(max-width: 470px) {
    width: 65vw;
  }
`;

interface TextProps {
  display: boolean;
}

const Error = styled.p<TextProps>`
  color: red;
  margin: 10px;
  display: ${(props) => props.display ? 'block' : 'none'}
`;


interface State {
  email: string;
  password: string;
  displayErrorMessage: boolean;
}

interface Props extends RouteComponentProps {
}

class LoginScreen extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      displayErrorMessage: false,
    }
  }

  onSubmit = () => {
    const {
      email,
      password,
    } = this.state;
    if (email === authUser.email && password === authUser.password) {
      localStorage.setItem('credentials', JSON.stringify({
        email,
        password,
      }));
      this.props.history.push('/profile');
    } else {
      this.setState({
        displayErrorMessage: true,
      })
    }
  }
  render() {
    return (
      <HomePageBackground>
        <LoginCard>
          <Input
            placeholder='Email: www.example.com'
            type='text'
            onChange={(event: ChangeEvent<HTMLInputElement>) => this.setState({
              email: event.target.value,
            })}
          />
          <Input
            placeholder='Password: ********'
            type='password'
            onChange={(event: ChangeEvent<HTMLInputElement>) => this.setState({
              password: event.target.value,
            })}
          />
          <Error
            display={this.state.displayErrorMessage}
          >
            Wrong credentials
          </Error>
          <Button
            onClick={this.onSubmit}
            type={'submit'}
          >
            Login
          </Button>
        </LoginCard>
      </HomePageBackground>
    );
  }
}

export default withRouter(LoginScreen);