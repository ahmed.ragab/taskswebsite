import React from 'react';
import styled from 'styled-components';
import {
  HomePageBackground,
} from '../../components/sharedComponents';

const LoginCard = styled.div`
  width: 30vw;
  height: auto;
  border-radius: 5px;
  padding: 20px;
  background-color: white;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  @media(max-width: 465px) {
    width: 70vw;
  }
`;

const Info = styled.p`
  font-size: 16;
  color: black;
`;


class ProfileScreen extends React.Component {
  render () {
    return (
      <HomePageBackground>
        <LoginCard>
          <Info>Username: Admin</Info>
          <Info>Name: Ahmed</Info>
          <Info>Age: 24</Info>
          <Info>Job: Web Developer</Info>
        </LoginCard>
      </HomePageBackground>
    );
  }
}

export default ProfileScreen;