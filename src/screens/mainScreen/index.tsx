import React from 'react';
import styled from 'styled-components';
import image from '../../assets/images/todo.jpg';

const HomePageBackground = styled.div`
  background-image: url('${image}');
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;
  background-attachment: fixed;
  width: 100%;
  height: 100vh;
`;

class MainScreen extends React.Component {

  render() {
    return (
      <HomePageBackground/>
    );
  }
}

export default MainScreen;