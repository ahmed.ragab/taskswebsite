import React from 'react';
import AddTaskForm from '../components/addTaskForm';
import {
  configure,
  mount,
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { TaskPayload } from '../stores/types';

configure({ adapter: new Adapter() })

describe("<AddTaskForm />", () => {
  it("should display a blank add task form", async () => {
    const wrapper = mount(<AddTaskForm handleAddTask={(task: TaskPayload) =>  {}}/>);

    // test labels
    const labels = wrapper.find('label');
    expect(labels).toHaveLength(2);
    expect(labels.at(0).text()).toEqual('Category :');
    expect(labels.at(1).text()).toEqual('Task :');

    // test input fields
    const inputs = wrapper.find('input');
    expect(inputs).toHaveLength(2);

    const catgeroyInput = inputs.at(0);
    catgeroyInput.simulate('change', { target: { value: 'Sports' } })
    expect(wrapper.state('category')).toEqual('Sports');

    const taskInput = inputs.at(1);
    taskInput.simulate('change', { target: { value: 'Task 5' } })
    expect(wrapper.state('task')).toEqual('Task 5');

    // test button
    const button = wrapper.find('button');
    expect(button.prop('type')).toEqual('submit');
    button.simulate('submit');
    expect(wrapper.state('category')).toEqual('');
    expect(wrapper.state('task')).toEqual('');
  });
});