import React from 'react';
import LoginScreen from '../screens/loginScreen';
import {
  configure,
  mount,
  shallow,
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { BrowserRouter as Router } from 'react-router-dom';

configure({ adapter: new Adapter() })

describe("<LoginScreen />", () => {
  it("should display a blank login form", async () => {
    const wrapper = mount(
        <Router>
          <LoginScreen/>
        </Router>
    );

    const loginScreen = wrapper.find('LoginScreen');

    // test input fields
    const inputs = wrapper.find('input');
    expect(inputs).toHaveLength(2);

    const emailInput = inputs.at(0);
    emailInput.simulate('change', { target: { value: 'Admin' } });
    expect(loginScreen.state('email')).toEqual('Admin');

    const passwordInput = inputs.at(1);
    passwordInput.simulate('change', { target: { value: '543210' } })
    expect(loginScreen.state('password')).toEqual('543210');

    // test button
    const button = wrapper.find('button');
    expect(button.prop('type')).toEqual('submit');
    button.simulate('click');
    expect(loginScreen.state('displayErrorMessage')).toEqual(true);

  });
});